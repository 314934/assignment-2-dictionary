%define WSIZE 8

global find_word
extern string_length
extern string_equals

section .text

find_word:
    xor rax, rax
    .loop:
        test rsi, rsi
        jz .error
        push rsi
        push rdi
        add rsi, WSIZE
        call string_equals
        pop rdi
        pop rsi
        cmp rax, 1
        jz .end
        mov rsi, [rsi]
        jmp .loop
    .end:
        mov rax, rsi
        ret
    .error:
        xor rax, rax
        ret
