%define NEWLINE_CHAR 10

section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60 ; Первый аргумент, код возврата, передается через rdi и в syscall тоже
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rdi, 1
    mov rax, 1
    syscall
    ret
    

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE_CHAR
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, rsp ; сохраняем значение стека
    mov r9, 10  ; делитель
    mov rax, rdi ; делимое
    push 0 ; null-terminator
    .loop:
        xor rdx, rdx
        div r9
        add dl, "0" ; переводим число в символ
        dec rsp
        mov byte[rsp], dl
        test rax, rax
        jnz .loop
    mov rdi, rsp ; указатель на начало строки
    push r8
    call print_string
    pop r8
    mov rsp, r8 ; восстанавливаем значение стека
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jns .end
    neg rdi
    push rdi
    mov rdi, "-"
    call print_char
    pop rdi
    .end:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
        xor rax, rax                    
        mov al, byte[rdi]             
        cmp al, byte[rsi]            
        jne .noteq                   
        cmp al, 0                    
        je .eq                       
        inc rdi                
        inc rsi               
        jmp .loop                 
    .noteq:
        xor rax, rax                    
        ret
    .eq:
        mov rax, 1                   
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rsi, rsp
    mov rax, 0
    mov rdi, 0
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push rdi
    mov r13, rsi ; buff-size left
    mov r12, rdi ; address next
    .loop_start:
        call read_char
        cmp rax, 0x20
        je .loop_start
        cmp rax, 0x9
        je .loop_start
        cmp rax, 0xA
        je .loop_start
    .loop_chars:
        cmp rax, 0
        je .final
        cmp rax, 0x20
        je .final
        cmp rax, 0x9
        je .final
        cmp rax, 0xA
        je .final
        dec r13
        cmp r13, 0
        jbe .error
        mov byte[r12], al
        inc r12
        call read_char
        jmp .loop_chars
    .final:
        mov byte[r12], 0
        mov rdi, [rsp]
        call string_length
        mov rdx, rax
        pop rax
        pop r13
        pop r12
        ret
    .error:
        pop rax
        pop r13
        pop r12
        xor rax, rax
        ret
        

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8
    mov r9, 10
    .loop:
        mov r8b, [rdi+rdx]
        test r8b, r8b
        je .end
        cmp r8, "0"
        jb .end
        cmp r8, "9"
        ja .end
        push rdx
        mul r9
        pop rdx
        sub r8, "0"
        add rax, r8
        inc rdx
        jmp .loop
    .end:
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, byte[rdi]
    cmp rax, '-'
    je .neg
    ; мы делаем jmp, а не call, потому что так обработка продолжиться как для безнакового числа
    ; и ret в parse_uint сделает возврат к вызывающему коду
    jmp parse_uint 
    .neg:
        inc rdi
        call parse_int
        test rdx, rdx
        je .error
        neg rax
        inc rdx
    ret 
    .error:
        xor rdx, rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rcx
    call string_length
    pop rcx
    pop rdi
    cmp rax, rdx
    jae .error
    mov rcx, rax
    .loop:
        test rcx, rcx
        je .end
        dec rcx
        mov dl, byte [rdi + rcx]
        mov byte [rsi + rcx], dl
        jmp .loop
    .end:
        mov byte [rsi + rax], 0
        ret
    .error:
        xor rax, rax
        ret   

