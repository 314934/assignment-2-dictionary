ASM=nasm
ASMFLAGS=-f elf64
LD=ld

program: main.o dict.o lib.o
	$(LD) -o program $^
%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<
clean:
	$(RM) *.o
	$(RM) program

.PHONY: clean
