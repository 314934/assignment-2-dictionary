%include "words.inc"
%include "lib.inc"

%define BUFFER_SIZE 256
%define WSIZE 8

extern find_word
global _start

section .rodata

buffer_error: db "Error! Buffer", 0
key_error: db "Error! Key", 0

section .bss

buffer: times BUFFER_SIZE db 0

section .text

_start:
    xor rax, rax
    mov rdi, buffer
    mov rsi, BUFFER_SIZE
    call read_word
    test rax, rax
    jz .buffer_overflow

    mov rdi, rax
    mov rsi, NEXTE
    push rdx
    call find_word
    pop rdx
    test rax, rax
    je .wrong_key

    add rax, rdx
    add rax, WSIZE
    inc rax
    mov rdi, rax
    call print_string
    call print_newline
    xor rdi, rdi
    call exit

    .buffer_overflow:
        mov rdi, buffer_error
        call print_string
        mov rdi, 1
        call exit
    .wrong_key:
        mov rdi, key_error
        call print_string
        mov rdi, 1
        call exit
