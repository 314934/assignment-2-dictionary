%define NEXTE 0

%macro colon 2
    %ifid %2
        %2: dq NEXTE
        %define NEXTE %2
    %else
        %fatal "Error! Argument 2 must be ID"
    %endif
    
    %ifstr %1
        db %1, 0
    %else
        %fatal "Error! Argument 1 must be String"
    %endif
%endmacro
